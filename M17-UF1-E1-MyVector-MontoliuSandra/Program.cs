using System;

// Programa per comprovar els exercicis.
public class Program
{
    public static void Main()
    {
        MyVector myVector = new MyVector();


        // Exercici 1

        Console.WriteLine("--------------------- Exercici 1 ------------------");
        Hello.AskName();



        // Exercici 2

        Console.WriteLine("\n\n--------------------- Exercici 2 ------------------");
        MyVector vector1 = new MyVector(2, 2.5, 8.9, 10);
        Console.WriteLine("Mostra el vector creat:\n  " + vector1);
        Console.WriteLine("\nCrida només alguns dels parámetres de l'objecte:");
        Console.WriteLine("  Posició punt inicial vector1: { " + vector1.GetXPuntInici() + " , " + vector1.GetYPuntInici() + " }");
        vector1.SetYPuntInici(5);
        Console.WriteLine("\nMostra la nova posició del vector després de canviar una de les coordenades:\n  " + vector1);
        Console.WriteLine("\nDistancia entre les dues cordenades del vector: " + myVector.DistanciaVector(vector1));
        vector1.ChangeDirection();
        Console.WriteLine("\nVector canviat de sentit:\n  " + vector1);



        // Exercici 3

        Console.WriteLine("\n\n--------------------- Exercici 3 ------------------");
        VectorGame vectorGame = new VectorGame();
        MyVector[] vectors = VectorGame.RandomVectors(5);
        Console.WriteLine("Mostra els vectors creats de manera aleatoria:");
        vectorGame.WriteVectors(vectors);
        VectorGame.SortVectors(vectors, true);
        Console.WriteLine("\nMostrar els vectors ordenats per longitud:");
        vectorGame.WriteVectors(vectors);
        VectorGame.SortVectors(vectors, false);
        Console.WriteLine("\nMostrar els vectors ordenats per distancia al punt (0,0):");
        vectorGame.WriteVectors2(vectors);

        Console.WriteLine("\nMostrar vectors visualment: ");
        MyVector vector5 = new MyVector(1, 2, 5, 10);
        Console.WriteLine(vector5);
        vectorGame.VisualVector(vector5);
        MyVector vector6 = new MyVector(10, 20, 2, 4);
        Console.WriteLine("\n" + vector6);
        vectorGame.VisualVector(vector6);
        MyVector vector7 = new MyVector(3, 8, 3, 4);
        Console.WriteLine("\n" + vector7);
        vectorGame.VisualVector(vector7);
        MyVector vector10 = new MyVector(5, 8, 5, 11);
        Console.WriteLine("\n" + vector10);
        vectorGame.VisualVector(vector10);
        MyVector vector8 = new MyVector(-2,3,-3,2);
        Console.WriteLine("\n" + vector8);
        vectorGame.VisualVector(vector8);
        MyVector vector9 = new MyVector(-3,2,-2,3);
        Console.WriteLine("\n" + vector9);
        vectorGame.VisualVector(vector9);
    }
}


// Exercici 1: Introducció Hello {name}
// Pregunta el nom a l'usuari i saluda'l.
public class Hello
{
    public static void AskName()
    {
        Console.Write("Introdueix el teu nom: ");
        string name = Console.ReadLine();
        Console.WriteLine("Hello " + name);
    }
}

// Exercici 2: Classe MyVector
// Crea i gestiona vectors en un pla de dos dimensions
public class MyVector
{

    // Variable on es guraden les coordenades.
    public double[,] CoordenadesVector;

    // Crear vector com objecte.
    public MyVector(double xInici, double yInici, double xFinal, double yFinal)
    {
        CoordenadesVector = new[,]
        {
            { xInici, yInici },
            { xFinal, yFinal }
        };
    }

    public MyVector(MyVector vector)
    {
        CoordenadesVector = new[,]
        {
            { vector.GetXPuntInici(), vector.GetYPuntInici() },
            { vector.GetXPuntFinal(), vector.GetYPuntFinal() }
        };
    }

    public MyVector()
    {

    }

    // Sets i Gets de cada punt d'un vector.
    public void SetXPuntInici(double xInici)
    {
        CoordenadesVector[0, 0] = xInici;
    }

    public double GetXPuntInici()
    {
        return CoordenadesVector[0, 0];
    }

    public void SetYPuntInici(double yInici)
    {
        CoordenadesVector[0, 1] = yInici;
    }

    public double GetYPuntInici()
    {
        return CoordenadesVector[0, 1];
    }

    public void SetXPuntFinal(double xFinal)
    {
        CoordenadesVector[1, 0] = xFinal;
    }

    public double GetXPuntFinal()
    {
        return CoordenadesVector[1, 0];
    }

    public void SetYPuntFinal(double yFinal)
    {
        CoordenadesVector[1, 1] = yFinal;
    }

    public double GetYPuntFinal()
    {
        return CoordenadesVector[1, 1];
    }

    // Calacular distancia del vector
    //        P(x,y)  P2(x2,y2)
    // (x2 - x)^2 + (y2 - y)^2 = distancia^2
    //  
    public double DistanciaVector(MyVector vector)
    {
        double costatX = Math.Pow(vector.GetXPuntFinal() - vector.GetXPuntInici(), 2);
        double costatY = Math.Pow(vector.GetYPuntFinal() - vector.GetYPuntInici(), 2);
        double distancia = Math.Sqrt(costatX + costatY);
        return distancia;
    }

    // Canviar sentit del vector
    public void ChangeDirection()
    {
        double xInici = GetXPuntInici();
        double yInici = GetYPuntInici();

        SetXPuntInici(GetXPuntFinal());
        SetYPuntInici(GetYPuntFinal());
        SetXPuntFinal(xInici);
        SetYPuntFinal(yInici);

    }

    // ToString
    public override string ToString()
    {
        return "V { { " + CoordenadesVector[0, 0] + " , " + CoordenadesVector[0, 1] + " } , { " + CoordenadesVector[1, 0] + " , " + CoordenadesVector[1, 1] + " } }";
    }
}

// Exercici 3: Classe VectorGame
public class VectorGame
{

    // Retorna array amb vectors aleatoris, passar llargada per paràmetre.
    public static MyVector[] RandomVectors(int length)
    {
        MyVector[] vectors = new MyVector[length];

        for (int i = 0; i < length; i++)
        {
            vectors[i] = CreateVector();
        }

        return vectors;
    }

    // Métode que crea un vector aleatori.
    public static MyVector CreateVector()
    {
        Random rnd = new Random();

        double x1 = Math.Round(rnd.Next(-50, 50) + rnd.NextDouble(), 2);
        double y1 = Math.Round(rnd.Next(-50, 50) + rnd.NextDouble(), 2);
        double x2 = Math.Round(rnd.Next(-50, 50) + rnd.NextDouble(), 2);
        double y2 = Math.Round(rnd.Next(-50, 50) + rnd.NextDouble(), 2);

        MyVector vectorRandom = new MyVector(x1, y1, x2, y2);

        return vectorRandom;
    }

    // Ordena array segons distancia de vectors o proximitat a l'origen. Passa per paràmetre array i booleà segons tipus d'ordre.
    public static void SortVectors(MyVector[] vectors, bool ordreVectors)
    {
        MyVector myVector = new MyVector();
        // ordreVectors = true  -> Ordre per distancia
        if (ordreVectors == true)
        {
            for (int i = 0, j = 0; j < Math.Pow(vectors.Length,2); i++, j++)
            {
                if (i == vectors.Length - 1) i = 0;
                if (myVector.DistanciaVector(vectors[i]) > myVector.DistanciaVector(vectors[i + 1]))
                {
                    MyVector vectorAux = new MyVector(vectors[i]);
                    vectors[i] = vectors[i + 1];
                    vectors[i + 1] = vectorAux;
                }
            }
        }
        // ordreVectors = false -> Ordre per proximitat a (0,0)
        else
        {
            for (int i = 0, j = 0; j < Math.Pow(vectors.Length,2); i++, j++)
            {
                if (i == vectors.Length - 1) i = 0;
                if (DistanciaEix(vectors[i]) > DistanciaEix(vectors[i + 1]))
                {
                    MyVector vectorAux = new MyVector(vectors[i]);
                    vectors[i] = vectors[i + 1];
                    vectors[i + 1] = vectorAux;
                }
            }
        }
    }

    public static double DistanciaEix(MyVector vector)
    {
        MyVector myVector = new MyVector();

        MyVector distanciaPuntI = new MyVector(0, 0, vector.GetXPuntInici(), vector.GetYPuntInici());
        MyVector distanciaPuntF = new MyVector(0, 0, vector.GetXPuntFinal(), vector.GetYPuntFinal());

        bool mesProper = myVector.DistanciaVector(distanciaPuntI) < myVector.DistanciaVector(distanciaPuntF);
        // MesProper = true  -> El punt inicial es el més proper a (0,0).
        // MesProper = false -> El punt final del vector es el més proper.
        MyVector vectorAux;
        if (mesProper)
        {
            return myVector.DistanciaVector(distanciaPuntI);
        }
        return myVector.DistanciaVector(distanciaPuntF);
    }

    // Escriu cada un dels vectors d'un array més la seva longitud.
    public void WriteVectors(MyVector[] vectors)
    {
        MyVector myVector = new MyVector();

        for (int i = 0; i < vectors.Length; i++)
        {
            Console.WriteLine(vectors[i] + " Distancia: " + myVector.DistanciaVector(vectors[i]));
        }
    }

    // Escriu cada un dels vectors d'un array més la distancia entre l'eix .
    public void WriteVectors2(MyVector[] vectors)
    {
        MyVector myVector = new MyVector();

        for (int i = 0; i < vectors.Length; i++)
        {
            Console.WriteLine(vectors[i] + " Distancia eix: " + DistanciaEix(vectors[i]));
        }
    }

    // Dibuixa els vectors.
    public void VisualVector(MyVector vector)
    {
        string[,] matriu =
        {
            {" "," "," "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "," "," "}
        };
        double pendent = 0;

        // Descobrir on comença el vector
        if (vector.GetXPuntInici() < vector.GetXPuntFinal())
        { // Inici a l'esquerra, final dreta.
            if (vector.GetYPuntInici() < vector.GetYPuntFinal())
            { // Inici abaix, final a dalt.
                matriu[9, 0] = "·";
            }
            else
            { // Inici a dalt, final abaix.
                matriu[0, 0] = "·";
            }
            pendent = (vector.GetXPuntFinal() - vector.GetXPuntInici()) / (vector.GetYPuntFinal() - vector.GetYPuntInici());
        }
        else if (vector.GetXPuntInici() == vector.GetXPuntFinal())
        {
            for (int i = 0; i < matriu.GetLength(0); i++) matriu[i, 0] = "·";
            if (vector.GetYPuntInici() < vector.GetYPuntFinal()) matriu[9, 0] = "V";
            else matriu[0, 0] = "A";
        }
        else
        { // Inici dreta, final esquerra.
            if (vector.GetYPuntInici() < vector.GetYPuntFinal())
            { // Inici abaix, final a dalt.
                matriu[9, 9] = "·";
            }
            else
            { // Inici a dalt, final abaix.
                matriu[0, 9] = "·";
            }
            pendent = (vector.GetXPuntInici() - vector.GetXPuntFinal()) / (vector.GetYPuntInici() - vector.GetYPuntFinal());
        }

        // Dibuixar segons la pendent del vector.
        if (pendent == 0.5)
        {
            if (vector.GetXPuntInici() < vector.GetXPuntFinal())
            { // Inici a l'esquerra, final dreta.
              // Inici abaix, final a dalt. ------ Funciona
                    matriu[1, 8] = "·"; matriu[2, 7] = "·"; matriu[3, 6] = "·"; matriu[4, 5] = "·"; matriu[5, 4] = "·"; matriu[6, 3] = "·"; matriu[7, 2] = "·"; matriu[8, 1] = "·"; matriu[9, 0] = "·";
                    matriu[0, 9] = "7"; // Simbol per a la fletxa
            }
            else
            { // Inici dreta, final esquerra.
              // Inici a dalt, final abaix. ------ Funciona
                matriu[1, 8] = "·"; matriu[2, 7] = "·"; matriu[3, 6] = "·"; matriu[4, 5] = "·"; matriu[5, 4] = "·"; matriu[6, 3] = "·"; matriu[7, 2] = "·"; matriu[8, 1] = "·"; matriu[0, 9] = "·";
                matriu[9, 0] = "L"; // Simbol per a la fletxa
               
            }
        }
        else if (pendent == 1)
        {
            if (vector.GetXPuntInici() < vector.GetXPuntFinal())
            { // Inici a l'esquerra, final dreta.
                /// Inici a dalt, final abaix.
                matriu[1, 8] = "·"; matriu[2, 7] = "·"; matriu[3, 6] = "·"; matriu[4, 5] = "·"; matriu[5, 4] = "·"; matriu[6, 3] = "·"; matriu[7, 2] = "·"; matriu[8, 1] = "·"; matriu[9, 0] = "·";
                matriu[0, 9] = "7"; // Simbol per a la fletxa
                
            }
            else
            {   // Inici dreta, final esquerra.
                // Inici abaix, final a dalt.
                matriu[1, 8] = "·"; matriu[2, 7] = "·"; matriu[3, 6] = "·"; matriu[4, 5] = "·"; matriu[5, 4] = "·"; matriu[6, 3] = "·"; matriu[7, 2] = "·"; matriu[8, 1] = "·"; matriu[0, 9] = "·";
                matriu[9, 0] = "L"; // Simbol per a la fletxa

            }
        }
        else
        {
            //No acabat.
        }

        Console.WriteLine("+--------------+");
        for (int i = 0; i < matriu.GetLength(0); i++)
        {
            for (int j = 0; j < matriu.GetLength(1); j++)
            {
                if (j == 0) Console.Write("|  ");
                Console.Write(matriu[i, j]);
                if (j == 9 && i != 10) Console.WriteLine("  |");
            }
        }
        Console.WriteLine("+--------------+");
    }
}

